﻿using CityInfo.API.Service;
using CityInfo.API.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace CityInfo.API.Controllers
{
    [Route("api/cities")]
    public class CityController : Controller
    {
        private ICityInfoRepository _cityInfoRepository;

        public CityController(ICityInfoRepository cityInfoRepository)
        {
            _cityInfoRepository = cityInfoRepository;
        }

        [HttpGet()]
        public IActionResult GetCities()
        {
            var cityEntities = _cityInfoRepository.GetCities();
            var results = Mapper.Map<IEnumerable<CityWithoutPointOfInterestDto>>(cityEntities);
            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetCitie(int id, bool inludePointOfInterest = false)
        {
            var city = _cityInfoRepository.GetCity(id, inludePointOfInterest);

            if (city == null)
            {
                return NotFound();
            }

            if (inludePointOfInterest)
            {
                var cityResult = Mapper.Map<CityDto>(city);
                return Ok(cityResult);
            }

            var cityWithoutPointOfInterest = Mapper.Map<CityWithoutPointOfInterestDto>(city);
            return Ok(cityWithoutPointOfInterest);
        }
    }
}
