﻿using CityInfo.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityInfo.API
{
    public static class CityInfoContextExtension
    {
        public static void EnsureSeeDataForContext(this CityInfoContext context)
        {
            if (context.Cities.Any())
            {
                return;
            }

            //init seed data
            var cities = new List<City>()
            {
                new City()
                {
                    Name="New York City",
                    Description = "The one with that big park.",
                    PointOfInterest = new List<PointOfInterest>()
                    {
                        new PointOfInterest()
                        {
                            Name = "Central Park",
                            Description = "The most visited urban park in the United States." },
                        new PointOfInterest()
                        {
                            Name = "Emipre State Building",
                            Description = "A 102-story skyscraper located in Midtown Manhattan." },
                    }
                },
                new City()
                {
                    Name ="Antwerp",
                    Description = "The one with the cathedral that was never really finished.",
                    PointOfInterest = new List<PointOfInterest>()
                    {
                        new PointOfInterest()
                        {
                            Name = "Cathedral of Our Lady",
                            Description = "A Gothic style cathedral, conceived by architect Jan and Pieter Applemans." },
                        new PointOfInterest()
                        {
                            Name = "Antwerp Central Station",
                            Description = "The finest example of reailway atchitecture in Belguim." },
                    }
                },
                new City()
                {
                    Name ="Paris",
                    Description = "The one with that big tower.",
                    PointOfInterest = new List<PointOfInterest>()
                    {
                        new PointOfInterest()
                        {
                            Name = "Eiffel Tower",
                            Description = "A wrought iron lattice tower on the Cham de Mars, named after engineer Gustav Eiffel." },
                        new PointOfInterest()
                        {
                            Name = "The Louvre",
                            Description = "The world's largst museum." },
                    }
                }
            };

            context.Cities.AddRange(cities);
            context.SaveChanges();
        }
    }
}
